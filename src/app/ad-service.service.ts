import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, ObservableLike } from 'rxjs';
import { Item } from './ad-dashboard/Item';
import { Transaction } from './order-checkout/TransactionObject';
import { Ad } from './Ad';
import { RequestURL } from 'src/RequestURL';
@Injectable({
  providedIn: 'root'
})
export class AdServiceService {
  private url = new RequestURL();
  private showad_url = this.url.url_main_string+"/showAds";
  private postad_url = this.url.url_main_string+"/postAd";
  private soldad_url = this.url.url_main_string+"/updateAd/{}";
  private checkout_url = this.url.url_main_string+"/checkOutAd";
  private getTransId_url = this.url.url_main_string+"/getTransId";
  private searchAd = this.url.url_main_string + "/searchAd"; 
  
  constructor(private http:HttpClient) { }
  
  public postAd(ad : Ad){
    return this.http.post<Ad>(this.postad_url,ad);
  }
  public showAds():Observable<Item []>{
    return this.http.get<Item []>(this.showad_url);
  }
  public checkOutAd(transaction : Transaction){
    return this.http.post<String>(this.checkout_url,transaction); 
  }
  public getTransId(transaction : Transaction){
    return this.http.post<Transaction>(this.getTransId_url,transaction);
  }
  public soldAd(ad : String){
    return this.http.post<String>(this.soldad_url,ad);
  }
  public show():Observable<Item>{
    return this.http.get<Item>(this.showad_url);
  }
  public searchAdItem(value : string):Observable<Item []>{
    return this.http.post<Item []>(this.searchAd+"/"+value,null);
  }
}
